import pandas as pd
import numpy as np
import matplotlib . pyplot as plt

a=np.loadtxt('data.csv', skiprows=1, delimiter=',')
print(len(a))
print(f"Broj ljudi: {a.shape[0]}")

plt.scatter(a[:,1], a[:,2], c='b', s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase')
plt.show()

plt.scatter(a[::50,1], a[::50,2], c='b', s=1, marker=".")
plt.xlabel('Visina(cm)')
plt.ylabel('Masa(kg)')
plt.title('Odnos visine i mase(svaka 50-ta osoba)')
plt.show()

visina = a[:,1]
print(visina.max())
print(visina.min())
print(visina.mean())

musk = a[:,1][a[:,0]==1]
zene = a[:,1][a[:,0]==0]


print(f"najveca visina muskarca: {musk.max()}")
print(f"najmanja visina muskarca: {musk.min()}")
print(f"srednja visina muskaraca: {musk.mean()}")