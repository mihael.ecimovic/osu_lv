import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score, precision_score, recall_score
from tensorflow import keras
from keras import layers
from keras.models import load_model

data = np.genfromtxt('ispit/pima-indians-diabetes.csv', delimiter=',', skip_header=9)
#a
print(len(data))
#b
data_df = pd.DataFrame(data)
print(data_df)
bmi_duplicated = data_df[5].duplicated()
age_duplicated = data_df[7].duplicated()
bmi_null = data_df[data_df[5] == 0.0]
age_null = data_df[data_df[7] == 0.0]
data_df = data_df.drop_duplicates(subset=[5])
data_df = data_df.drop_duplicates(subset=[7])
data_df = data_df[data_df[5] != 0.0]
data_df = data_df[data_df[7] != 0.0]
print(f"Bmi duplicates:{len(bmi_duplicated[bmi_duplicated.values==True])}, age duplicates:{len(age_duplicated[age_duplicated.values==True])}, bmi null:{len(bmi_null)}, age null:{len(age_null)}\nLength:{len(data_df)}")
data = data_df.to_numpy()

#c
bmi = data[:, 5]
age = data[:, 7]
plt.scatter(bmi, age, s = 1)
plt.xlabel("BMI")
plt.ylabel("Age")
plt.title("Primjer")
plt.show()

#d
print(f"Max:{bmi.max()}, min:{bmi.min()}, mean:{bmi.mean()}\n")

#e
diabetes = data[np.where(data[:, 8] == 1.0)]
n_diabetes = data[np.where(data[:, 8] == 0.0)]
print(f"Max:{diabetes[:, 5].max()}, min:{diabetes[:, 5].min()}, mean:{diabetes[:, 5].mean()}\n")
print(f"Max:{n_diabetes[:, 5].max()}, min:{n_diabetes[:, 5].min()}, mean:{n_diabetes[:, 5].mean()}\n")

#2
data = np.genfromtxt('ispit/pima-indians-diabetes.csv', delimiter=',', skip_header=9)

X = data[:, 0:8]
y = data[:, 8]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

# #b
# y_test_p = LogRegression_model.predict(X_test)

#c
cm = confusion_matrix(y_test, y_test_p)
print ("Matrica zabune: ", cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_p))
disp.plot()
plt.show()

#d
print(f'Accuracy: {accuracy_score(y_test, y_test_p)}')
print(f'Precision: {precision_score(y_test, y_test_p)}')
print(f'Recall: {recall_score(y_test, y_test_p)}')

#3
data = np.genfromtxt('ispit/pima-indians-diabetes.csv', delimiter=',', skip_header=9)

X = data[:, 0:8]
y = data[:, 8]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a
model = keras.Sequential()
model.add(layers.Input(shape = (8,)))
model.add(layers.Dense(12, activation="relu"))
model.add(layers.Dense(8, activation="relu"))
model.add(layers.Dense(1, activation="sigmoid"))
model.summary()

#b
model.compile(loss="categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy", ])

#c
batch_size = 150
epochs = 10
history = model.fit(X_train,
                    y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_split=0.1)

#d
model.save("lab8/FCN/")
del model

#e
model = load_model("lab8/FCN")
score = model.evaluate(X_test, y_test, verbose = 0)

#f
y_test_p = model.predict(X_test)

cm = confusion_matrix(y_test, y_test_p.round())
print ("Matrica zabune: ", cm)
ConfusionMatrixDisplay(cm).plot()
plt.show()