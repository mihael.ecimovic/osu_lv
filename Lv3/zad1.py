import pandas as pd

data = pd.read_csv('Lv3/data_C02_emission.csv')

#a
#print(len(data))
#print(data.dtypes)
#print(data.duplicated().sum())
#print(data.isnull().sum())
data['Make']=pd.Categorical(data['Make'])
data['Vehicle Class']=pd.Categorical(data['Vehicle Class'])
data['Transmission']=pd.Categorical(data['Transmission'])
data['Fuel Type']=pd.Categorical(data['Fuel Type'])
data['Model']=pd.Categorical(data['Model'])
#print(data.dtypes)

#b
#new_data = data.sort_values(by="Fuel Consumption City (L/100km)")
#print(new_data.head(3)) #najmanja 3
#print(new_data.tail(3)) #najveca 3

#c
#print(data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)].agg({'Model':'count', 'CO2 Emissions (g/km)':'mean'}))

#d
#print(len(data[data['Make']=='Audi']))
#print(f"Prosjecna emisija CO2 audija s 4 cilindra: {(data[(data['Make']=='Audi') & (data['Cylinders']==4)])['CO2 Emissions (g/km)'].mean()} (g/km)")

#e
#print(data.groupby('Cylinders').agg({'Make':'count', 'CO2 Emissions (g/km)':'mean'}).rename(columns={'Make': 'Car count'}))

#f
print('Mean i median za dizel:')
print(data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].agg(['mean','median']))
print('Mean i median za benzin:')
print(data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].agg(['mean','median']))

#g
print('Vozilo s 4 cilindra dizel s najvećom potrošnjom u gradu:')
print(data[(data['Cylinders']==4) & (data['Fuel Type']=='D')].sort_values(by='Fuel Consumption City (L/100km)').tail(1))

