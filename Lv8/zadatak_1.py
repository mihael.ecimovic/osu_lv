import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import mnist
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))


(x_train, y_train), (x_test, y_test) = mnist.load_data()


# TODO: prikazi nekoliko slika iz train skupa
plt.imshow(x_train[5], cmap='gray')
plt.show()
plt.imshow(x_train[7], cmap='gray')
plt.show()
plt.imshow(x_train[2], cmap='gray')
plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential(
[
layers.Flatten(input_shape=(28,28,1)),
layers.Dense(128,activation="relu"),
layers.Dense(64,activation="relu"),
layers.Dense(10,activation="relu"),
]
)

model.summary()


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(loss="categorical_crossentropy",
optimizer="adam",
metrics=["accuracy",])


# TODO: provedi ucenje mreze
x_train = x_train.reshape(-1 ,28, 28, 1)
x_test = x_test.reshape(-1 ,28, 28, 1)


y_train = keras.utils.to_categorical(y_train,num_classes=10)
y_test = keras.utils.to_categorical(y_test,num_classes=10)

history = model.fit(x_train, y_train,
epochs=5,
batch_size=32,
validation_split=0.1)




# TODO: Prikazi test accuracy i matricu zabune
score = model.evaluate(x_test, y_test, verbose=0)
print(history.history)
print('Test accuracy:', score)

y_pred = model.predict(x_test_s)

y_true_labels = np.argmax(y_test_s, axis=1)
y_pred_classes = np.argmax(y_pred, axis=1)

cm = confusion_matrix(y_true_labels, y_pred_classes)
print(cm)
# TODO: spremi model

model.save("model")